(* Interprete sem_dynamic con scope dinamico *)
(* Gruppo id13. Componenti: Azzaro Edoardo, Garau Nicola, Lepori Massimiliano, Palla Mattia, Piras Cesare *)

(* Identificatori *)
type ide = string;;

(* Espressioni *)
type exp = 
    Eint of int 
  | Ebool of bool 
  | Bigint of int list
  | Castint of exp
  | Emptylist
  | Cons of exp * exp
  | Head of exp
  | Tail of exp
  | Den of ide
  | Prod of exp * exp
  | Sum of exp * exp
  | Diff of exp * exp
  | Mod of exp * exp
  | Div of exp * exp
  | Less of exp * exp
  | Eq of exp * exp
  | Iszero of exp
  | Or of exp * exp
  | And of exp * exp
  | Not of exp
  | Pair of exp * exp
  | Fst of exp
  | Snd of exp
  | Ifthenelse of exp * exp * exp
  | Let of (ide * exp) list * exp      
  | Fun of ide list * exp
  | Apply of exp * exp list;;

(* Tipi eval *)
type eval =
    Unbound
  | Evint of int
  | Evbool of bool
  | Evbigint of int list
  | Evlist of eval list
  | Evcons of exp*exp
  | Evpair of exp * exp
  | Evf of ide list * exp
  | Evfun of ide list * exp * env

 (* Dichiarazione ambiente *)
 and env = Env of (ide -> eval);;

(* Eccezione Identificatore Unbound *)
exception UnboundIde of ide;;

(* Funzione che crea un ambiente vuoto *)
let emptyenv= fun()-> Env(fun x -> Unbound);;

(* Funzione per l'associazione identificatori-valori, restituisce un ambiente *)
let bind (Env rho) x d = Env (fun y -> if y=x then d else rho y);;

(* Come per la bind, solo che *)
let rec bindlist (amb,il,el) = match (il,el) with
  | ([],[]) -> amb
  | i::il1, e::el1 -> bindlist ((
				 match e with
				   Evf(id,e) -> bind amb i (Evfun(id,e,amb))
				 | _ -> bind amb i e), il1, el1)
  | _ -> failwith "Errore bindlist";;

(* Restituisce il valore di una variabile in un ambiente *)
let applyenv (Env rho) x = match rho x with
    Unbound -> raise (UnboundIde x)
  | _ as d -> d;;

(* Controllo dei tipi *)
let typecheck (variabile, tipo) = match tipo with
    "intero"   -> (match variabile with Evint(i) -> true |_ -> false)
  | "booleano" -> (match variabile with Evbool(b) -> true |_ -> false)
  | "bigint"   -> (match variabile with Evbigint(bi) -> true |_ -> false)
  | "emptylist"-> (match variabile with Evlist([])-> true |_ -> false)     
  | "lista"    -> (match variabile with Evlist(l) -> true |_ -> false)
  | "coppia"   -> (match variabile with Evpair(l1, l2) -> true |_ -> false)
  | _          -> failwith("Tipo non riconosciuto");;

(* Restituisce il tipo di una variabile *)
let returnType variabile = match variabile with
    Evint(i) -> "intero"
  | Evbool(b) -> "booleano"
  | Evbigint(bi) ->"bigint"
  | Evlist([])-> "emptylist"     
  | Evlist(l) -> "lista"
  | Evpair(l1, l2) -> "coppia"
  | _          -> failwith("Tipo non riconosciuto");;

(* Restituisce true se negativo e false altrimenti *)
let isNegative (n) = if( typecheck(n,"intero") ) then (
		       match n with
			 Evint(num) -> if(num<0) then true else false
		     )
		     else (if(typecheck(n,"bigint")) then (
			     match n with
			       Evbigint(hd::tl) -> if(hd<0) then true else false
			   )
			   else failwith ("Operandi non interi"));;

(* Calcolo del valore assoluto, restituisce un tipo eval *)
let absoluteValue (n) = if( typecheck(n,"intero") ) then (
			  match n with
			    Evint(num) -> Evint(abs(num))
			)
			else (if(typecheck(n,"bigint")) then (
				match n with
				  Evbigint(hd::tl) -> Evbigint(abs(hd)::tl)
			      )
			      else failwith ("Operandi non interi"));;

(* Calcolo del valore assoluto, restituisce una lista *)
let absoluteValueB (n) = if(typecheck(n,"bigint")) then (
			   match n with
			     Evbigint(hd::tl) -> (abs(hd)::tl))
			 else failwith ("Operando non bigint");;

(* Serve per il cast da int a Bigint *)
let rec cast (n) = if( typecheck(n,"intero")) then
		     (match n with
			Evint(n1) when n1<(-9)  -> cast(Evint(n1/10))@[abs(n1 mod 10)]
		      | Evint(n1) when n1<0  -> [n1]
		      | Evint(n1) when n1<10 -> [n1]
		      | Evint(n1)            -> cast(Evint(n1/10))@[n1 mod 10]
		      | _                    -> failwith "Errore match cast")
		   else failwith ("Operando non intero");;

(* Confronto tra due interi/bigint *)
let lesser (n1,n2) = if( typecheck(n1,"intero") && typecheck(n2,"intero")) then n1<n2
		     else
		       (if( typecheck(n1,"bigint") && typecheck(n2,"bigint"))
			then match (n1,n2) with 
			       Evbigint(np),Evbigint(ns) -> if (List.length(np)<List.length(ns)) then true 
							    else (if ((List.length np) > (List.length ns) ) then false else np<ns) 
			else (if(typecheck(n1,"bigint") && typecheck(n2,"intero"))
			      then n1<Evbigint(cast(n2))
			      else if(typecheck(n1,"intero") && typecheck(n2,"bigint"))
			      then Evbigint(cast(n1))<n2
			      else failwith ("Operandi non interi")));;

(* Crea una lista a partire da una coppia *)
let createlist (e1,e2) =
  match e1,e2 with
    Evint(p),Evint(s)       -> [Evint(p)]@[Evint(s)]
  | Evbool(p),Evbool(s)     -> [Evbool(p)]@[Evbool(s)]
  | Evbigint(p),Evbigint(s) -> [Evbigint(p)]@[Evbigint(s)]
  | Evlist(p),Evint(s)      -> if p = [] || typecheck (List.hd p,"intero") 
                               then e2::p else failwith "Errore. Sono stati inseriti tipi diversi"
  | Evlist(p),Evbool(s)     -> if p = [] || typecheck (List.hd p,"booleano") then e2::p 
                               else failwith "Errore. Sono stati inseriti tipi diversi"
  | Evlist(p),Evbigint(s)   -> if p = [] || typecheck (List.hd p,"bigint") 
                               then e2::p else failwith "Errore. Sono stati inseriti tipi diversi"
  | Evlist(p),Evlist(s)     -> if p = [] || s = [] || returnType (List.hd p)=returnType (List.hd s) then p @ s 
                               else failwith "Errore. Sono stati inseriti tipi diversi"
  | Evlist(p),Evpair(s,z)   -> if p = [] || typecheck (List.hd p,"coppia") then e2::p 
                               else failwith "Errore. Sono stati inseriti tipi diversi"
  | Evint(p),Evlist(s)      -> if s = [] || typecheck (List.hd s,"intero") then e1::s 
                               else failwith "Errore. Sono stati inseriti tipi diversi"
  | Evbool(p),Evlist(s)     -> if s = [] || typecheck (List.hd s,"booleano") then e1::s 
                               else failwith "Errore. Sono stati inseriti tipi diversi"
  | Evbigint(p),Evlist(s)   -> if s = [] || typecheck (List.hd s,"bigint") then e1::s 
                               else failwith "Errore. Sono stati inseriti tipi diversi"
  | Evpair(p,z),Evlist(s)   -> if s = [] || typecheck (List.hd s,"coppia") then e1::s 
                               else failwith "Errore. Sono stati inseriti tipi diversi"
  | Evpair(p1,p2),Evpair(s1,s2) -> [Evpair(p1,p2)]@[Evpair(s1,s2)]
  | _ -> failwith "Riscontrati tipi diversi nella lista."
;;


(* Restituisce la testa di una lista *)
let gethead(e) = if( typecheck(e,"lista") ) then match e with
                                                   Evlist([]) -> Unbound
						 | Evlist(l) -> List.hd l
		 else failwith "Il parametro non è una lista";;

(* Restituisce la coda di una lista *)
let gettail(e) = if( typecheck(e,"lista") ) then match e with
                                                   Evlist([]) -> Unbound
						 | Evlist(l) -> Evlist(List.tl l)
		 else failwith "Il parametro non è una lista";;

(* Toglie gli zeri a sinistra di un numero (codificato come lista, es [1;2;3] vale 123) *)
let rec toglizeri(l) = match l with
    [] -> [0]
  | hd::tl -> if(hd=0) then toglizeri(tl) else l;;
  
(* Inverte il segno di un numero (codificato come lista, es [1;2;3] vale 123) *)
let cambiaSegno(l) = match l with
    hd::tl -> (hd*(-1))::tl
  | _ -> failwith"Errore cambiaSegno";;		      			      

(* Esegue la sottrazione di due numeri codificati come lista *)
let sottrailiste (l1,l2) = 
  let rec sottrailisteProvaR (l1,l2,risultato, prestito) = match (l1,l2)with
      (hd1::[]), [] when (prestito = 0) -> (hd1-0)::risultato
    | (hd1::[]), [] when (prestito = 1) -> (hd1-1)::risultato
    | (l1,[])  when (prestito = 0)      -> List.rev(l1)@risultato
    | (l1,[])  when (prestito = 1)      -> sottrailisteProvaR(l1,[1],[],0)@risultato
    | (hd1::tl1, hd2::tl2) when (hd1-hd2<0 && prestito = 0)           -> 
       sottrailisteProvaR (tl1,tl2,[hd1-hd2+10]@risultato,1)
    | (hd1::tl1, hd2::tl2) when (hd1-hd2-prestito<0 && prestito=1)    -> 
       sottrailisteProvaR (tl1,tl2,[hd1-hd2+9]@risultato,1)
    | (hd1::tl1, hd2::tl2) when (hd1-hd2>=0 && prestito = 0)          -> 
       sottrailisteProvaR((tl1,tl2,[hd1-hd2]@risultato, 0)) 	 
    | (hd1::tl1, hd2::tl2) when (hd1-hd2-prestito>=0 && prestito = 1) -> 
       sottrailisteProvaR((tl1,tl2,[hd1-hd2-prestito]@risultato, 0))
  in toglizeri(sottrailisteProvaR(List.rev(l1),List.rev(l2),[], 0));;

(* Esegue la somma di due numeri codificati come lista *)
let sommaliste(l1,l2) =
  let rec sommalisteR(l1,l2,riporto) = match (l1,l2) with
      ([],[]) -> (riporto::[])
    | (hd1::tl1, []) -> (sommalisteR(l1,[riporto],0))
    | ([], hd2::tl2) -> (sommalisteR(l2,[riporto],0))
    | (hd1::tl1, hd2::tl2) -> (match(hd1+hd2+riporto) with
				 n when n<10 -> (sommalisteR(tl1,tl2,0))@[hd1+hd2+riporto]
			       | _           -> (sommalisteR(tl1,tl2,1))@[(hd1+hd2+riporto) mod 10])
  in toglizeri(sommalisteR(List.rev l1, List.rev l2,0));;

(* Gestisce il segno nella sottrazione tra due liste (richiama sottraiListe per l'operazione base) *)
let sottraiSegnoListe(l1,l2) = match (l1,l2) with
    (a,b) when (not (isNegative(Evbigint(l1)))) && (not (isNegative(Evbigint(l2))))
    -> if(lesser(absoluteValue(Evbigint(l2)), absoluteValue(Evbigint(l1))))
       then sottrailiste(l1,l2)
       else cambiaSegno(sottrailiste(l2,l1)) (*CAMBIARE SEGNO*)
  | (a,b) when (not (isNegative(Evbigint(l1)))) && ((isNegative(Evbigint(l2))))
    -> sommaliste(absoluteValueB(Evbigint(l1)),absoluteValueB(Evbigint(l2)))
  |  (a,b) when ((isNegative(Evbigint(l1)))) && (not (isNegative(Evbigint(l2))))
     -> cambiaSegno(sommaliste(absoluteValueB(Evbigint(l1)),absoluteValueB(Evbigint(l2)))) (*CAMBIARE SEGNO*) 
  |  (a,b) when ((isNegative(Evbigint(l1)))) && ((isNegative(Evbigint(l2))))
     -> if(lesser(absoluteValue(Evbigint(l2)), absoluteValue(Evbigint(l1))))
	then cambiaSegno(sottrailiste(absoluteValueB(Evbigint(l1)),absoluteValueB(Evbigint(l2)))) (*CAMBIARE SEGNO*)
	else sottrailiste(absoluteValueB(Evbigint(l2)),absoluteValueB(Evbigint(l1)))
  | _ -> failwith "Caso inesistente";;

(* Gestisce il controllo dei tipi nella sottrazione, richiama sottraiSegnoListe *)
let sottrai(n1,n2) = if( typecheck(n1,"intero") && typecheck(n2,"intero"))
		     then match(n1,n2) with
			    (Evint(p),Evint(s)) -> Evint(p-s)
		     else
		       (if( typecheck(n1,"bigint") && typecheck(n2,"bigint"))
			then match(n1,n2) with
			       (Evbigint(p),Evbigint(s)) ->
			       (
				 match (p,s) with
				   (l1,l2) -> Evbigint(sottraiSegnoListe(l1,l2))
			       )
			else (if(typecheck(n1,"bigint") && typecheck(n2,"intero"))
			      then match(n1) with
				     (Evbigint(p)) -> Evbigint(sottraiSegnoListe(p,cast(n2)))
			      else if(typecheck(n1,"intero") && typecheck(n2,"bigint")) (* aggiungere meno *)
			      then match(n2) with
				     (Evbigint(s)) -> Evbigint(sottraiSegnoListe(cast(n1),s))
			      else failwith ("Operandi non interi")));;

(* Gestisce il segno nella somma tra due liste (richiama sommaliste per l'operazione base) *)
let sommaSegno(l1,l2) = match (l1,l2) with
    (a,b) when (not (isNegative(Evbigint(l1)))) && (not (isNegative(Evbigint(l2))))
    -> sommaliste(l1 , l2)
  | (a,b) when (not (isNegative(Evbigint(l1)))) && ((isNegative(Evbigint(l2))))
    -> sottraiSegnoListe(l1 ,absoluteValueB(Evbigint(l2)))
  |  (a,b) when ((isNegative(Evbigint(l1)))) && (not (isNegative(Evbigint(l2))))
     -> cambiaSegno(sottraiSegnoListe(absoluteValueB(Evbigint(l1)) , l2))
  |  (a,b) when ((isNegative(Evbigint(l1)))) && ((isNegative(Evbigint(l2))))
     -> cambiaSegno(sommaliste(absoluteValueB(Evbigint(l1)) , absoluteValueB(Evbigint(l2))))
  | _ -> failwith "Caso inesistente";;

(* Gestisce il controllo dei tipi nella somma, richiama sommaSegno *)
let somma(n1,n2) = if( typecheck(n1,"intero") && typecheck(n2,"intero"))
		   then match(n1,n2) with
			  (Evint(p),Evint(s)) -> Evint(p+s)
		   else
		     (if( typecheck(n1,"bigint") && typecheck(n2,"bigint"))
		      then match(n1,n2) with
			     (Evbigint(p),Evbigint(s)) -> Evbigint(sommaSegno(p,s))
		      else (if(typecheck(n1,"bigint") && typecheck(n2,"intero"))
			    then match(n1) with
				   (Evbigint(p)) -> Evbigint(sommaSegno(p,cast(n2)))
			    else if(typecheck(n1,"intero") && typecheck(n2,"bigint"))
			    then match(n2) with
				   (Evbigint(s)) -> Evbigint(sommaSegno(cast(n1),s))
			    else failwith ("Operandi non interi")));;

(* Funzione ausiliaria di moltiplicaArray, gestisce il riporto nella moltiplicazione *)
let moltiplicaMap(le,n) = match (le*n) with
    tot when tot<10 -> [tot]
  | tot             -> [(tot)/10; (tot) mod 10];;

(* Funzione ausiliaria di moltiplicaliste, moltiplica una cifra per un numero codificato come lista *)
let moltiplicaArray(l,n)= 
  let rec moltiplicaArrayR(l,n,risultato,zeri) = match l with
      []     -> risultato
    | hd::tl -> moltiplicaArrayR(tl, n, (sommaliste(risultato,(moltiplicaMap(hd,n))@zeri)), zeri@[0])
  in moltiplicaArrayR(List.rev l,n,[],[]);;

(* Esegue la moltiplicazione tra due numeri codificati come liste *)
let moltiplicaliste(l1,l2) =
  if(l1=[] || l2=[]) then failwith "Parametri non validi" else
    (
      let rec moltiplicalisteR(l1,l2,risParz,zeri) =
	match l2 with
	  []     -> risParz
	| hd::tl -> moltiplicalisteR(l1,tl,sommaliste(risParz,moltiplicaArray(l1,hd)@zeri),zeri@[0])
      in moltiplicalisteR(l1, List.rev l2, [], [])
    );;

(* Gestisce il controllo dei tipi nella moltiplicazione, richiama moltiplicaliste *)
let moltiplica(n1,n2) = if( typecheck(n1,"intero") && typecheck(n2,"intero"))
			then match(n1,n2) with
			       (Evint(p),Evint(s)) -> Evint(p*s)
			else
			  (if( typecheck(n1,"bigint") && typecheck(n2,"bigint"))
			   then match(n1,n2) with
				  (Evbigint(p),Evbigint(s))
				  -> if((isNegative(n1) && (not(isNegative(n2))))
					|| (not (isNegative(n1)) && isNegative(n2)))
				     then Evbigint(cambiaSegno(moltiplicaliste(absoluteValueB(n1),absoluteValueB(n2))))
				     else Evbigint(moltiplicaliste(absoluteValueB(n1),absoluteValueB(n2)))
			   else (if(typecheck(n1,"bigint") && typecheck(n2,"intero"))
				 then (
				   match(n1) with
				     (Evbigint(p))
				     -> if((isNegative(n1) && (not(isNegative(n2))))
					   || (not (isNegative(n1)) && isNegative(n2)))
					then Evbigint(cambiaSegno(moltiplicaliste(absoluteValueB(n1),cast(absoluteValue(n2)))))
					else Evbigint(moltiplicaliste(absoluteValueB(n1),cast(absoluteValue(n2))))
				 )
				 else if(typecheck(n1,"intero") && typecheck(n2,"bigint"))
				 then (
				   match(n2) with
				     (Evbigint(s))
				     -> if((isNegative(n1) && (not(isNegative(n2))))
					   || (not (isNegative(n1)) && isNegative(n2)))
					then Evbigint(cambiaSegno(moltiplicaliste(cast(absoluteValue(n1)),absoluteValueB(n2))))
					else Evbigint(moltiplicaliste(cast(absoluteValue(n1)),absoluteValueB(n2)))
				 )
				 else failwith ("Operandi non interi")));;



  

(* Esegue la divisione tra due numeri codificati come liste *)
let dividiliste(l1,l2) =
  let rec dividilisteR(l1, l2, risultato) = match (l1,l2) with
      ([],[]) -> failwith "Parametri vuoti"
    | ([],l2) -> failwith "Dividendo vuoto"
    | (l1,[]) -> failwith "Errore dividiliste"
    | (l1,l2) -> if(List.length l1 < List.length l2
		    || (List.length l1 = List.length l2) && l1<l2)
		 then risultato
		 else
		   (
		     if((List.length l1 = List.length l2) && l1=l2)
		     then sommaliste(risultato, [1])
		     else dividilisteR(sottrailiste(l1,l2), l2, sommaliste(risultato, [1]))
		   )
  in dividilisteR(l1, l2, [0]);;

(* Gestisce il controllo dei tipi nella divisione, richiama dividiliste *)
let dividi(n1,n2) = if( typecheck(n1,"intero") && typecheck(n2,"intero"))
		    then match(n1,n2) with
			   (Evint(p),Evint(s)) -> Evint(p/s)
		    else
		      (if( typecheck(n1,"bigint") && typecheck(n2,"bigint"))
		       then match(n1,n2) with
			      (Evbigint(p),Evbigint(s))
			      -> if((isNegative(n1) && (not(isNegative(n2))))
				    || (not (isNegative(n1)) && isNegative(n2)))
				 then Evbigint(cambiaSegno(dividiliste(absoluteValueB(n1),absoluteValueB(n2))))
				 else Evbigint(dividiliste(absoluteValueB(n1),absoluteValueB(n2)))
		       else (if(typecheck(n1,"bigint") && typecheck(n2,"intero"))
			     then (
			       match(n1) with
				 (Evbigint(p))
				 -> if((isNegative(n1) && (not(isNegative(n2))))
				       || (not (isNegative(n1)) && isNegative(n2)))
				    then Evbigint(cambiaSegno(dividiliste(absoluteValueB(n1),cast(absoluteValue(n2)))))
				    else Evbigint(dividiliste(absoluteValueB(n1),cast(absoluteValue(n2))))
			     )
			     else if(typecheck(n1,"intero") && typecheck(n2,"bigint"))
			     then (
			       match(n2) with
				 (Evbigint(s))
				 -> if((isNegative(n1) && (not(isNegative(n2))))
				       || (not (isNegative(n1)) && isNegative(n2)))
				    then Evbigint(cambiaSegno(dividiliste(cast(absoluteValue(n1)),absoluteValueB(n2))))
				    else Evbigint(dividiliste(cast(absoluteValue(n1)),absoluteValueB(n2)))
			     )
			     else failwith ("Operandi non interi")));;

(* Controlla se due variabili sono equivalenti, restituisce un booleano *)
let equals (n1,n2) = if((typecheck(n1,"intero") && typecheck(n2,"intero"))
			|| (typecheck(n1,"booleano") && typecheck(n2,"booleano"))
			|| (typecheck(n1,"bigint") && typecheck(n2,"bigint"))
			|| (typecheck(n1,"lista") && typecheck(n2,"lista"))) then n1=n2
		     else ( if(typecheck(n1,"intero") && typecheck(n2,"bigint"))then (Evbigint(cast(n1))=n2) 
			    else (if (typecheck(n1,"bigint") && typecheck(n2,"intero")) then (n1=Evbigint(cast(n2)))
				  else failwith "Operandi non corretti"));;
(* Controlla se un numero è equivalente a zero, restituisce un booleano *)
let zero (n) = if( typecheck(n,"intero") ) then n=Evint(0)
	       else (if(typecheck(n,"bigint")) then n=Evbigint([0])
		     else failwith ("Operandi non interi"));;

(* Or logico tra due booleani *)
let orTemp (b1,b2) = if( typecheck(b1,"booleano") && typecheck(b2,"booleano")) then
		       (match (b1,b2) with
     			| Evbool(b1), Evbool(b2) -> Evbool(b1||b2) )
		     else failwith ("Operandi non booleani");;

(* And logico tra due booleani *)
let andTemp (b1,b2) = if( typecheck(b1,"booleano") && typecheck(b2,"booleano")) then
			(match (b1,b2) with
     			 | Evbool(b1), Evbool(b2) -> Evbool(b1&&b2) )
		      else failwith ("Operandi non booleani");;

(* Negazione (Not) di un booleano *)
let negation (b) = if(typecheck(b,"booleano")) then (match b with
     						     | Evbool(b1) -> Evbool(not b1) )
		   else failwith ("Operando non booleano");;

(* Restituisce il primo elemento di una coppia, gestisce anche coppie di liste e coppie di coppie *)
let getFirst (p) = if(typecheck(p,"coppia"))
		   then (match p with
			   Evpair(primo,secondo) -> (match primo with
						       Eint(p) -> Evint(p)
						     | Ebool(p) -> Evbool(p)
						     | Bigint(p) -> Evbigint(p)
						     | Emptylist -> Evlist([])
						     | Cons(p1,p2) -> Evcons(p1,p2)
						     | Pair(p1,p2) -> Evpair(p1,p2)
						     | _ -> failwith("Errore match getFirst"))
			  |_ -> failwith("Errore match getFirst (esterno)"))
		   else failwith ("Operando non coppia");;

(* Restituisce il secondo elemento di una coppia, gestisce anche coppie di liste e coppie di coppie *)
let getSecond (p) = if(typecheck(p,"coppia"))
		    then (match p with
			    Evpair(primo,secondo) -> (match secondo with
							Eint(p) -> Evint(p)
						      | Ebool(p) -> Evbool(p)
						      | Bigint(p) -> Evbigint(p)
						      | Emptylist -> Evlist([])
						      | Cons(p1,p2) -> Evcons(p1,p2)
						      | Pair(p1,p2) -> Evpair(p1,p2)
						      | _ -> failwith("Errore match getSecond"))
			   |_ -> failwith("Errore match getSecond (esterno)"))
		    else failwith ("Operando non coppia");;

(* Controlla i tipi, funzione d'appoggio per l'ifthenelse *)
let evaluateif(c,e1,e2) = if(typecheck(c,"booleano")) then
			    (match c with
			       Evbool(b) -> if(b) then e1 else e2 
			     | _ -> failwith ("Errore match evaluateif"))
			  else failwith ("Operando non booleano o espressioni di tipo diverso");;

(* Non la usiamo *)
(* let rec delimita (iel,e,rho) = match iel with *)
(*     [] -> e *)
(*   | hd::tl -> delimita(tl,e,(bind rho (fst hd) (snd hd)));; *)

(* let rho:env = emptyenv();; *)

(* Interprete con scope dinamico *)
let rec sem_dynamic exp rho = match exp with
    Eint(i) -> Evint(i)
  | Ebool(b) -> Evbool(b)
  | Bigint(il) -> Evbigint(il)
  | Castint(i) -> Evbigint(cast(sem_dynamic i rho))
  | Emptylist -> Evlist([])
  | Cons(e1,e2) -> Evlist(createlist(sem_dynamic(e1) rho, sem_dynamic(e2) rho))
  | Head(e) -> gethead(sem_dynamic e rho)
  | Tail(e) -> gettail(sem_dynamic e rho)
  | Den(i) -> applyenv (rho) i
  | Prod (e1,e2) -> moltiplica((sem_dynamic e1 rho), (sem_dynamic e2 rho))
  | Sum (e1,e2) -> somma((sem_dynamic e1 rho), (sem_dynamic e2 rho))
  | Diff (e1,e2) -> sottrai((sem_dynamic e1 rho), (sem_dynamic e2 rho)) 
  | Mod (e1,e2) -> sottrai(sem_dynamic e1 rho ,moltiplica(sem_dynamic e2 rho, dividi((sem_dynamic e1 rho), (sem_dynamic e2 rho))))
  | Div (e1,e2) -> dividi((sem_dynamic e1 rho), (sem_dynamic e2 rho))
  | Less (e1,e2) -> Evbool(lesser((sem_dynamic e1 rho), (sem_dynamic e2 rho)))
  | Eq (e1,e2) -> Evbool(equals((sem_dynamic e1 rho), (sem_dynamic e2 rho)))
  | Iszero (e) -> Evbool(zero(sem_dynamic e rho))
  | Or (e1,e2) -> orTemp((sem_dynamic e1 rho), (sem_dynamic e2 rho))
  | And (e1,e2) -> andTemp((sem_dynamic e1 rho), (sem_dynamic e2 rho))
  | Not (e) -> (negation(sem_dynamic e rho))
  | Pair(e1,e2) -> Evpair(e1,e2)
  | Fst(e) -> getFirst(sem_dynamic e rho)
  | Snd(e) -> getSecond(sem_dynamic e rho)
  | Ifthenelse(cond,e1,e2) ->  let ite = sem_dynamic cond rho in
                               if typecheck(ite,"booleano")
			       then
                                 (if ite = Evbool(true)
                                  then sem_dynamic e1 rho
                                  else sem_dynamic e2 rho)
                               else failwith ("Condizione non booleana (Ifthenelse)")
  | Let(l,e) -> let rho1 = List.fold_right (fun a b -> bind b (fst a) (sem_dynamic (snd a) rho)) l rho in sem_dynamic e rho1
  | Fun(il,e) -> Evf(il,e)
  | Apply(f, el) ->
     ( match sem_dynamic f rho with
         Evf(il,eTemp) -> let rho1 = 
			    bindlist(rho, il,(semlist el rho)) 
			  in sem_dynamic eTemp rho1
       | Evfun(il,eTemp,rho1) -> let rho2 = 
				   bindlist(rho1, il,(semlist el rho)) 
				 in sem_dynamic eTemp rho2
       | _ -> failwith "Errore Apply sem_dynamic"   
     )

(* Funzione ausiliaria per la Apply *)
and semlist el rho = match el with
  | [] -> []
  | e::el1 -> (sem_dynamic e rho) :: (semlist el1 rho)

(* Altra funzione ausiliaria per la Apply *)
and applyTemp pf pa rhoTemp = match (pf,pa) with 
    ([],_) -> rhoTemp
  | (hd::tl,[]) -> applyTemp tl [] (bind (rhoTemp) (hd) Unbound)
  | (hd1::tl1,hd2::tl2) -> applyTemp tl1 tl2 (bind (rhoTemp) (hd1) (sem_dynamic (hd2) rhoTemp));;
